# Nunet webapp

nunet compute provider webapp

## Installation

installed om your device
You need to have [Node.js](https://nodejs.org/en/download/) installed on your device.

then navigate to the base dir of the project and run in your terminal

```bash
npm install
```

```bash
npm run build
```

## Usage

then copy the server.py and the tests.py files located in the project base dir into the newly generated build dir

then open a terminal in this pass and run the server.py file

## The Deb(Linux) Package

to run the deb package all you have to do is to download it on your linux device and run

```bash
sudo dpkg -i <filename>
```

## License

[Nunet](https://www.nunet.io/)
