import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AppProvider } from "./store/provider";
import { AppForm } from "./Form";
import { Page404 } from "./pages/Page404";

function App() {
  return (
    <AppProvider>
      <BrowserRouter>
        <Routes>
          {/* <Route path="/" element={<SocketContainer />} /> */}
          <Route path="/" element={<AppForm />} />
          {/* <Route path="/dms" element={<NetwrokChanger />} /> */}
          <Route path="/*" element={<Page404 />} />
        </Routes>
      </BrowserRouter>
    </AppProvider>
  );
}

export default App;
