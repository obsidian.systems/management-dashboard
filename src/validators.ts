export const UrlValidator = /^(ftp|http|https):\/\/[^ "]+$/;
export const IpValidator =
  /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/;
