import React, { useState, useContext } from "react";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { ToastContainer, toast } from "react-toastify";
import { BACKEND_ENDPOINT } from "./constants";
import { AppForm } from "./Form";
import { Result } from "./pages/Result";
import { AppContext } from "./store/context";
import { useLocalStorage } from "./hooks/useLocalStorage";

export function SocketContainer() {
  const [isResult, setIsResult] = useState(false);
  const [link, setLink] = useState("");
  const [Ssocket, setSocket] = useState({});

  const state = useContext(AppContext);
  const [ip, setIp] = useLocalStorage("ip", state.ip);

  React.useEffect(() => {
    const url = ip + BACKEND_ENDPOINT;
    console.log(">>> Trying to connect to: " + url);
    let socket;
    try {
      socket = new W3CWebSocket(url);
    } catch (error) {
      toast.info("Connection Failed!", {
        position: "bottom-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    }
    socket.onerror = (e) => {
      toast.info(e, {
        position: "bottom-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    };
    socket.onopen = function () {
      socket.onmessage = (msg) => {
        if (msg.data[0] === "{") {
          const data = JSON.parse(msg.data);
          console.log(data);
          if (data["action"] === "deployment-response") {
            setIsResult(true);
            console.log("YES IT SHOULD BE TRUE");
            setLink(data.message.content);
            console.log(data.message.content);
            console.log(data);
          }
        } else {
          setIsResult(false);
          setSocket(socket);
        }
      };
    };
  }, []);

  return (
    <>{isResult ? <Result link={link} /> : <AppForm socket={Ssocket} />}</>
  );
}
