import React from "react";
import "./Alert.css";

export const Alert = ({ text }) => <div className="alert_box">{text}</div>;
