import axios from "axios";
import { BACKEND_ENDPOINT } from "../constants";

const ObjectFactory = (address: String) => {
  return {
    ComputeProviderAddress: address,
  };
};

export const getClaim = async (address: String) => {
  const res = await axios.post(BACKEND_ENDPOINT, ObjectFactory(address));
  return res.data;
};
