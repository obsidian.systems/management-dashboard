import React, { useState, useEffect } from "react";
import ReactLoading from "react-loading";

export function NunetLoader({ duration, children, onFinsih, ...props }) {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
      onFinsih();
    }, duration);
  }, []);

  if (loading) {
    return (
      <div
        style={{
          position: "absolute",
          left: "50%",
          top: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        <ReactLoading {...props} />
      </div>
    );
  }

  return <div>{children}</div>;
}

NunetLoader.defaultProps = {
  color: "rgb(0,167,157)",
  delay: 0,
  duration: 1300,
  height: 40,
  width: 40,
  type: "bars",
};
