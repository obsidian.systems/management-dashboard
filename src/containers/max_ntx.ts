export function Calculate_Static_NTX_GPU(resource_usage, time) {
  const Estimated_Computation_Time = time;
  let Minimum_Estimated_NTX = 0;
  if (resource_usage === "Low") {
    const NTX_MINUTE = 0.00008;
    const NTX_WATT_MINUTE = 0.00016;
    const WATTS = 170;
    Minimum_Estimated_NTX =
      NTX_MINUTE * Estimated_Computation_Time +
      NTX_WATT_MINUTE * WATTS * Estimated_Computation_Time;
  }
  if (resource_usage === "Moderate") {
    const NTX_MINUTE = 0.00016;
    const NTX_WATT_MINUTE = 0.00016;
    const WATTS = 220;
    Minimum_Estimated_NTX =
      NTX_MINUTE * Estimated_Computation_Time +
      NTX_WATT_MINUTE * WATTS * Estimated_Computation_Time;
  }
  if (resource_usage === "High") {
    const NTX_MINUTE = 0.00032;
    const NTX_WATT_MINUTE = 0.00016;
    const WATTS = 350;
    Minimum_Estimated_NTX =
      NTX_MINUTE * Estimated_Computation_Time +
      NTX_WATT_MINUTE * WATTS * Estimated_Computation_Time;
  }

  return Minimum_Estimated_NTX;
}
