import { PYTORCH_REGISTRY, TENSORFLOW_REGISTRY } from "../constants";

const dict = { Tensorflow: TENSORFLOW_REGISTRY, Pytorch: PYTORCH_REGISTRY };

export const imageMapper = (t) => dict[t];
