import { Button, Header, Navbar } from "rsuite";
import ArowBack from "@rsuite/icons/ArowBack";
import { useNavigate } from "react-router-dom";
import Logo from "../logo.png";
import "./Result.css";

export const Result = ({ link }) => {
  let navigate = useNavigate();
  return (
    <>
      <Header>
        <Navbar
          appearance="inverse"
          style={{ background: "#0a2042", position: "fixed", width: "100%" }}
        >
          <Navbar.Brand style={{ textAlign: "left" }}>
            <span style={{ color: "#fff" }}>
              <img
                src={Logo}
                alt="logo"
                style={{
                  width: "100px",
                  marginTop: "-5px",
                  textAlign: "left",
                }}
              />
            </span>
          </Navbar.Brand>
        </Navbar>
      </Header>
      <div className="Result">
        <h3 style={{ color: "white" }}>Check the Result of the Process at</h3>
        <a style={{ color: "lightblue" }} href={link}>
          {link}
        </a>
      </div>
    </>
  );
};
