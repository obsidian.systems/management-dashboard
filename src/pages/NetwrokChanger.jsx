import React, { useState, useContext } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Navbar,
  Panel,
  FlexboxGrid,
  Whisper,
  Tooltip,
  Button,
  ButtonToolbar,
  IconButton,
} from "rsuite";
import { useNavigate } from "react-router-dom";

import Logo from "../logo.png";

import { NunetLoader } from "../components/NunetLoader";
import { useLocalStorage } from "hooks/useLocalStorage";
import { AppContext } from "../store/context";
import { SocketContainer } from "SocketContainer";

export function NetwrokChanger() {
  const state = useContext(AppContext);

  const [ip, setIp] = useLocalStorage("ip", state.ip);

  const [link, setLink] = useState(ip);

  const [done, setDone] = useState(false);

  const navigate = useNavigate();

  const onSubmit = () => {
    let cleanedLink = link.trim();
    if (cleanedLink[cleanedLink.length - 1] === "/") {
      cleanedLink = cleanedLink.slice(0, -1);
    }

    if (cleanedLink === "") {
      cleanedLink = "";
    } else if (!cleanedLink.includes("://")) {
      cleanedLink = "ws://" + cleanedLink;
    }

    if (cleanedLink.includes("http")) {
      cleanedLink = "ws" + cleanedLink.slice(4);
    }

    console.log(cleanedLink);
    setIp(cleanedLink);
    setDone(true);
  };

  return !done ? (
    <NunetLoader duration={2000} onFinsih={() => {}}>
      <div className="show-fake-browser login-page">
        <Container>
          <Header>
            <Navbar appearance="inverse" style={{ background: "#0a2042" }}>
              <Navbar.Brand style={{ textAlign: "left" }}>
                <span style={{ color: "#fff" }}>
                  <img
                    src={Logo}
                    alt="logo"
                    style={{
                      width: "100px",
                      marginTop: "-5px",
                      textAlign: "left",
                    }}
                  />
                </span>
              </Navbar.Brand>
            </Navbar>
          </Header>
          <Content>
            <FlexboxGrid justify="center" style={{ margin: "2rem 0" }}>
              <FlexboxGrid.Item colspan={12} className="flex-panel-container">
                <Panel
                  style={{ background: "white", padding: "10px" }}
                  className="flex-panel"
                  header={
                    <h4
                      style={{
                        textAlign: "center",
                        fontWeight: "300",
                        marginBottom: "20px",
                      }}
                    >
                      Change The DMS URL
                      <hr />
                    </h4>
                  }
                  shaded
                >
                  <Form fluid onSubmit={() => onSubmit()}>
                    <Form.Group>
                      <Form.ControlLabel style={{ position: "relative" }}>
                        The IP address and Port
                      </Form.ControlLabel>
                      <Form.Control
                        value={link}
                        name="ip"
                        placeholder="If left Empty, then the dms is on the same webapp address and port"
                        onChange={(v) => setLink(v.replace(/\s/gi, ""))}
                      />
                    </Form.Group>
                    <Form.Group>
                      <ButtonToolbar>
                        <Button
                          appearance="ghost"
                          color="green"
                          size="lg"
                          style={{ width: "27.5%" }}
                          onClick={() => setLink(state.ip)}
                        >
                          Reset
                        </Button>
                        <Button
                          appearance="primary"
                          size="lg"
                          style={{ width: "70%" }}
                          type="submit"
                        >
                          Proceed
                        </Button>
                      </ButtonToolbar>
                    </Form.Group>
                  </Form>
                </Panel>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Content>
        </Container>
      </div>
    </NunetLoader>
  ) : (
    <SocketContainer />
  );
}
